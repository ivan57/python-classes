#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gzip
import json
import configparser
import argparse
import re
from string import Template
# from json import encoder
import logging
import os
from datetime import datetime
import sys
import shutil
from collections import namedtuple
from typing import Pattern

# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';


MAX_ERROR_COUNT = 100
MAX_ERROR_PERCENT = 0.1

config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log",
    "LOGGING_TO_FILE": True
}

FLOAT_PRECITION = 3
LogFile = namedtuple('LogFile', ['filename', 'date'])


def read_log(file):
    """
    Iterator for reading file line by line
    :param file:
    :return:
    """
    while True:
        line = file.readline()
        if not line:
            break
        yield line


def median(lst):
    n = len(lst)
    if n < 1:
            return None
    if n % 2 == 1:
            return sorted(lst)[n//2]
    else:
            return sum(sorted(lst)[n//2-1:n//2+1])/2.0


def get_last_log_filename(log_dir):
    """
    Searching youngest log file mathing template nginx-access-ui.log-<YearMonthDay> or .gz
    :param log_dir:
    :return:
    """
    log_filename = ''
    last_log_date = 0
    log_file_regexp = re.compile(r"""^nginx-access-ui.log-(?P<date>[0-9]{8})(.gz)?""")  # type: Pattern[str]
    if not os.path.isdir(log_dir):
        logging.error('LOG_DIR="%s" not found!' % log_dir)
        return False
        # raise FileNotFoundError('Directory "%s" is not found!' % log_dir)

    for dir_entry in os.scandir(log_dir):
        if dir_entry.is_file() and dir_entry.name.startswith('nginx-access-ui.log-'):
            matched = log_file_regexp.match(dir_entry.name)
            if matched and int(matched.group('date')) > last_log_date:
                last_log_date = int(matched.group('date'))
                log_filename = dir_entry.path
    if not log_filename:
        logging.info('Nginx log files not found in %s' % log_dir)
        return False
    return LogFile(filename=log_filename, date=last_log_date)


def main():

    try: # catching Ctrl+C or other Exception
        logging_filename = 'logfile.log' if config["LOGGING_TO_FILE"] else None
        logging.basicConfig(filename=logging_filename,
                            format="[%(asctime)s] %(levelname).1s %(message)s",
                            datefmt="%Y.%m.%d %H:%M:%S",
                            level=logging.DEBUG)
        logging.info('Starting NGINX Log Analyzer =======================================')

        logging.info('Pasrsing script args...')
        parser = argparse.ArgumentParser("log_analyzer.py")
        parser.add_argument('--config', help="config filename path")
        args = parser.parse_args()

        logging.info('Config section')
        configfile = configparser.ConfigParser(config)
        configfile.optionxform = str
        if args.config:
            configfile.read(args.config)
            configfile_dict = dict(configfile._sections['config'])
            config.update(configfile_dict) # Overwriting config options from file
            config['REPORT_SIZE'] = int(config['REPORT_SIZE'])
            logging.info('Using config: %s' % args.config)
        else:
            logging.info('Using default config')

        logging.debug(config)
        logging.info('Searching fresh log file')
        logfile = get_last_log_filename(config["LOG_DIR"])
        if not logfile:
            sys.exit(5)
        logging.info('Last log found: %s' % logfile.filename)

        logging.info('Parsing log filename and biuld report filename +Creating report directory if not exists')
        try:
            dt = datetime.strptime(str(logfile.date), "%Y%m%d")
        except ValueError:
            logging.error('Incorrect date in the log filename: ' + logfile.filename)
            sys.exit(1)

        # Check and create REPORT_DIR
        if not os.path.isdir(config['REPORT_DIR']):
            os.mkdir(config['REPORT_DIR'])

        # Check and copy file: jquery.tablesorter.min.js
        jquery_file_src = 'jquery.tablesorter.min.js'
        jquery_file_dest = os.path.join(config['REPORT_DIR'], jquery_file_src)
        if not os.path.isfile(jquery_file_dest):
            shutil.copyfile(jquery_file_src, jquery_file_dest)

        # Build report file path
        report_filename = os.path.join(config['REPORT_DIR'], "report-%s.html" % dt.strftime("%Y.%m.%d"))
        if os.path.exists(report_filename):
            logging.info('Report "%s" already exists!' % report_filename)
            sys.exit()

        logging.info('Reading and analyzing log file...')
        log = (gzip.open if logfile.filename.endswith('.gz') else open)(logfile.filename, 'rt', encoding='utf-8')
        urls = {}

        # log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
        #                     '$status $body_bytes_sent "$http_referer" '
        #                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
        #                     '$request_time';

        # s = '1.196.116.32 -  - [29/Jun/2017:03:50:22 +0300] ' \
        #     '"GET /api/v2/banner/25019354 HTTP/1.1" 200 927 "-" ' \
        #     '"Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" ' \
        #     '"-" "1498697422-2190034393-4708-9752759" "dc7161be3" 0.390'

        log_line_pattern = re.compile(r"^(?P<remote_host>[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}) +" 
                                      """(?P<remote_user>[^ ]+) +"""
                                      """(?P<http_x_real_ip>[^ ]+) +"""
                                      """(?P<time_local>\[[^\]]+\]) +"""
                                      """"[a-z]+ (?P<request>[^"]+) HTTP[^"]+" +"""
                                      """(?P<status>[^ ]+) +"""
                                      """(?P<body_bytes_sent>[^ ]+) +"""
                                      """"(?P<http_referer>[^"]+)" +"""
                                      """"(?P<http_user_agent>[^"]+)" +"""
                                      """"(?P<http_x_forwarded_for>[^"]+)" +"""
                                      """"(?P<http_X_REQUEST_ID>[^"]+)" +"""
                                      """"(?P<http_X_RB_USER>[^"]+)" +"""
                                      """(?P<request_time>[0-9]+\.[0-9]+)$""", re.IGNORECASE)

        # m = log_line_pattern.match(s)

        # if m:
        #     print(m.groups())
        # else:
        #     print('Error string!')

        line_count = 0
        error_count = 0
        total_request_count = 0
        total_request_time = 0.0

        for line in read_log(log):
            matches = log_line_pattern.match(line)
            if matches:
                url = matches.group("request")
                time = float(matches.group("request_time"))
                if url not in urls:
                    urls[url] = []
                urls[url].append(time)
                total_request_count += 1
                total_request_time += time
            else:
                error_count += 1
                if error_count > MAX_ERROR_COUNT:
                    logging.error('Too many error lines in log file - $s' % error_count)
                    sys.exit(4)
            line_count += 1
            # if line_count >= 500: break

        if line_count and error_count/line_count > MAX_ERROR_PERCENT:
            logging.error('Too many error lines (%.2f%%) in the log file' % (100*error_count/line_count))
            sys.exit(6)
        # print('Stat:', line_count, len(urls), error_count, total_request_count, total_request_time)
        # print(json.dumps(urls, indent=2))

        logging.info('Calculating statistics...')
        statistic = []
        # noinspection PyDictCreation
        for url, url_time in urls.items():

            url_stat = {'url': url,
                        'count': len(url_time),
                        'time_sum': round(sum(url_time), FLOAT_PRECITION)}

            # count percent
            url_stat['count_perc'] = round(100 * url_stat['count'] / total_request_count, FLOAT_PRECITION)

            # time_sum percent
            url_stat['time_perc'] = round(100 * url_stat['time_sum'] / total_request_time, FLOAT_PRECITION)

            # time avg, max, med
            url_stat['time_avg'] = round(url_stat['time_sum'] / url_stat['count'], FLOAT_PRECITION)
            url_stat['time_max'] = round(max(url_time), FLOAT_PRECITION)
            url_stat['time_med'] = round(median(url_time), FLOAT_PRECITION)

            statistic.append(url_stat)

        statistic.sort(key=lambda k: k['time_sum'], reverse=True)

        logging.info('Writing report file: %s', report_filename)

        # encoder.FLOAT_REPR = lambda o: format(o, '.3f')
        # json.encoder.FLOAT_REPR = lambda f: ("%.2f" % f)
        # TODO: It doesn't work! ;(((

        log = open('./report.html', 'rt')
        tpl_string = log.read()
        log.close()

        s = Template(tpl_string)
        report_string = s.safe_substitute(table_json=json.dumps(statistic[:config['REPORT_SIZE']]))
        log = open(report_filename, 'wt+')
        log.write(report_string)
        log.close()

        # {"count": 2767, "time_avg": 62.994999999999997, "time_max": 9843.5689999999995, "time_sum": 174306.35200000001,
        #    "url": "/api/v2/internal/html5/phantomjs/queue/?wait=1m", "time_med": 60.073, "time_perc": 9.0429999999999993,
        #    "count_perc": 0.106}

        logging.info('Done!')

    except KeyboardInterrupt:
        logging.exception("Interrapted by user!")
        sys.exit(2)
    except Exception as err:
        logging.exception(err)
        sys.exit(7)

if __name__ == "__main__":
    main()
